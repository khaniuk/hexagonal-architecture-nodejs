# Hexagonal Architecture Nodejs + Prisma ORM

## About Project

Project to manage user roles, using a hexagonal architecture structure (also known as ports and adapters).

## Docker

Create postgres container

```bash
docker-compose up -d
```

## Prisma

Execute prisma migration

```bash
npx prisma migrate dev                         //Develop
npx prisma migrate dev --name create_user      //Develop and specific name migration
npx prisma migrate deploy                      //Production
```

Open Prisma Studio

```bash
npx prisma studio
```

## Start and test

Start project

```bash
yarn dev
npm run dev
```

Test

```bash
yarn test
npm run test
```
