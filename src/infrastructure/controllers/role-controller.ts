import { FastifyReply, FastifyRequest } from "fastify";
import { z } from "zod";

import { ApplicationFactory } from "@/application/application-factory";
import { Role } from "@/domain/models/role";

export async function getRoles(request: FastifyRequest, reply: FastifyReply) {
  let roles: Array<Role>;

  try {
    const getRoleUseCase = ApplicationFactory.getRoleUseCase();
    roles = await getRoleUseCase.getAll();
  } catch (error) {
    if (error instanceof Error) {
      return reply.status(409).send({ message: error.message });
    }

    throw error;
  }

  return reply.status(200).send(roles);
}

export async function getRoleById(
  request: FastifyRequest,
  reply: FastifyReply
) {
  const roleParameterSchema = z.object({
    id: z.string(),
  });

  const { id } = roleParameterSchema.parse(request.params);

  let role: Role;

  try {
    const getRoleUseCase = ApplicationFactory.getRoleUseCase();
    role = await getRoleUseCase.getById(id);
  } catch (error) {
    if (error instanceof Error) {
      return reply.status(409).send({ message: error.message });
    }

    throw error;
  }

  return reply.status(200).send(role);
}

export async function createRole(request: FastifyRequest, reply: FastifyReply) {
  const createRoleBodySchema = z.object({
    description: z.string(),
  });

  const { description } = createRoleBodySchema.parse(request.body);

  let role: Role;

  try {
    const createRoleUseCase = ApplicationFactory.createRoleUseCase();
    role = await createRoleUseCase.create({
      id: "",
      description,
      status: true,
    });
  } catch (err) {
    if (err instanceof Error) {
      return reply.status(409).send({ message: err.message });
    }

    throw err;
  }

  return reply.status(201).send(role);
}

export async function updateRole(request: FastifyRequest, reply: FastifyReply) {
  const roleParameterSchema = z.object({
    id: z.string(),
  });

  const { id } = roleParameterSchema.parse(request.params);

  const updateRoleBodySchema = z.object({
    description: z.string(),
    status: z.boolean(),
  });

  const { description, status } = updateRoleBodySchema.parse(request.body);

  try {
    const updateRoleUseCase = ApplicationFactory.updateRoleUseCase();
    await updateRoleUseCase.update({
      id,
      description,
      status,
    });
  } catch (err) {
    if (err instanceof Error) {
      return reply.status(409).send({ message: err.message });
    }

    throw err;
  }

  return reply.status(200).send();
}

export async function deleteRole(request: FastifyRequest, reply: FastifyReply) {
  const roleParameterSchema = z.object({
    id: z.string(),
  });

  const { id } = roleParameterSchema.parse(request.params);

  try {
    const deleteRoleUseCase = ApplicationFactory.deleteRoleUseCase();
    await deleteRoleUseCase.delete(id);
  } catch (error) {
    if (error instanceof Error) {
      return reply.status(409).send({ message: error.message });
    }

    throw error;
  }

  return reply.status(401).send();
}
