import { PrismaRoleRepository } from "./prisma/prisma-role-repository";
import { PrismaUserRepository } from "./prisma/prisma-user-repository";

export class RepositoryFactory {
  public static roleRepository() {
    const roleRepository = new PrismaRoleRepository();

    return roleRepository;
  }

  public static userRepository() {
    const userRepository = new PrismaUserRepository();

    return userRepository;
  }
}
