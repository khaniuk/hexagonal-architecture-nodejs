import { prisma } from "@/lib/prisma";
import { Prisma } from "@prisma/client";

import { UserRepository } from "@/domain/ports/secondary/user-repository";

export class PrismaUserRepository implements UserRepository {
  async findAll() {
    const users = await prisma.user.findMany();

    return users;
  }

  async findById(id: string) {
    const user = await prisma.user.findUnique({
      where: {
        id,
      },
    });

    return user;
  }

  async findByEmail(email: string) {
    const user = await prisma.user.findUnique({
      where: {
        email,
      },
    });

    return user;
  }

  async create(data: Prisma.UserCreateInput) {
    const user = await prisma.user.create({
      data,
    });

    return user;
  }

  async update(id: string, data: Prisma.UserUpdateInput) {
    const userData = this.getUserData(data);

    const user = await prisma.user.update({
      where: {
        id: id,
      },
      data: {
        ...userData,
      },
    });

    return user;
  }

  private getUserData(user: Prisma.UserUpdateInput): Prisma.UserUpdateInput {
    let userData: Prisma.UserUpdateInput = {
      name: user.name,
      email: user.email,
      role: user.role,
      status: user.status,
    };

    if (user.password) {
      userData.password = user.password;
    }

    return userData;
  }

  async delete(id: string) {
    const user = await prisma.user.delete({
      where: {
        id: id,
      },
    });
  }
}
