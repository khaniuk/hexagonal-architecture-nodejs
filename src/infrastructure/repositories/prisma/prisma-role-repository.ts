import { prisma } from "@/lib/prisma";
import { Role } from "@prisma/client";

import { RoleRepository } from "@/domain/ports/secondary/role-repository";

export class PrismaRoleRepository implements RoleRepository {
  async findAll() {
    const roles = await prisma.role.findMany();

    return roles;
  }

  async findById(id: string) {
    const role = await prisma.role.findUnique({
      where: {
        id,
      },
    });

    return role;
  }

  async create(data: Role) {
    const role = await prisma.role.create({
      data,
    });

    return role;
  }

  async update(data: Role) {
    const role = await prisma.role.update({
      where: {
        id: data.id,
      },
      data,
    });

    return role;
  }

  async delete(id: string) {
    const role = await prisma.role.delete({
      where: {
        id,
      },
    });
  }
}
