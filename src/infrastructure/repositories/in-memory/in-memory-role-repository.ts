import { RoleRepository } from "@/domain/ports/secondary/role-repository";
import { Prisma, Role } from "@prisma/client";
import { randomUUID } from "node:crypto";

export class InMemoryRoleRepository implements RoleRepository {
  public roles: Role[] = [];

  async findAll() {
    const roles = this.roles;

    return roles;
  }

  async findById(id: string) {
    const role = this.roles.find((role) => role.id === id);

    if (!role) {
      return null;
    }

    return role;
  }

  async create(data: Prisma.RoleCreateInput) {
    const role: Role = {
      id: randomUUID(),
      description: data.description,
      status: true,
      created_at: new Date(),
      updated_at: new Date(),
    };

    this.roles.push(role);

    return role;
  }

  async update(data: Prisma.RoleUpdateInput) {
    const findIndex = this.roles.findIndex((role) => role.id == data.id);

    if (findIndex == -1) {
      throw Error("Role not found");
    }

    this.roles[findIndex].description = data.description + "";
    this.roles[findIndex].status = !!data.status;

    return this.roles[findIndex];
  }

  async delete(id: string) {
    const findIndex = this.roles.findIndex((role) => role.id == id);

    if (findIndex != -1) {
      this.roles.splice(findIndex, 1);
    }
  }
}
