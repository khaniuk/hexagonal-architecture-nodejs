import { UserRepository } from "@/domain/ports/secondary/user-repository";
import { Prisma, User } from "@prisma/client";
import { randomUUID } from "node:crypto";

export class InMemoryUserRepository implements UserRepository {
  public users: User[] = [];

  async findAll() {
    const roles = this.users;

    return roles;
  }

  async findById(id: string) {
    const user = this.users.find((item) => item.id === id);

    if (!user) {
      return null;
    }

    return user;
  }

  async findByEmail(email: string) {
    const user = this.users.find((item) => item.email === email);

    if (!user) {
      return null;
    }

    return user;
  }

  async create(data: Prisma.UserCreateInput) {
    const user: User = {
      id: randomUUID(),
      name: data.name,
      email: data.email,
      status: true,
      password: data.password,
      created_at: new Date(),
      role_id: randomUUID(),
    };

    this.users.push(user);

    return user;
  }

  async update(id: string, data: Prisma.UserCreateInput) {
    const findIndex = this.users.findIndex((user) => user.id == id);

    if (findIndex == -1) {
      throw Error("User not found");
    }

    this.users[findIndex].name = data.name + "";
    this.users[findIndex].email = data.email + "";
    this.users[findIndex].status = !!data.status;

    return this.users[findIndex];
  }

  async delete(id: string) {
    const findIndex = this.users.findIndex((user) => user.id == id);

    if (findIndex != -1) {
      this.users.splice(findIndex, 1);
    }
  }
}
