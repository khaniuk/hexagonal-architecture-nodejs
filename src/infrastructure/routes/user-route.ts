import { FastifyInstance } from "fastify";

import {
  createUser,
  deleteUser,
  getUserById,
  getUsers,
  updateUser,
} from "../controllers/user-controller";

export async function userRoute(app: FastifyInstance) {
  app.get("/users", getUsers);
  app.get("/users/:id", getUserById);
  app.post("/users", createUser);
  app.put("/users/:id", updateUser);
  app.delete("/users/:id", deleteUser);
}
