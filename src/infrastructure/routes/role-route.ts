import { FastifyInstance } from "fastify";

import {
  createRole,
  deleteRole,
  getRoleById,
  getRoles,
  updateRole,
} from "../controllers/role-controller";

export async function roleRoute(app: FastifyInstance) {
  app.get("/roles", getRoles);
  app.get("/roles/:id", getRoleById);
  app.post("/roles", createRole);
  app.put("/roles/:id", updateRole);
  app.delete("/roles/:id", deleteRole);
}
