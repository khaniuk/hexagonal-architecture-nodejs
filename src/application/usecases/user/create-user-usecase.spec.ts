import { InMemoryUserRepository } from "@/infrastructure/repositories/in-memory/in-memory-user-repository";
import { randomUUID } from "crypto";
import { beforeEach, describe, expect, it } from "vitest";
import { CreateUserUseCase } from "./create-user-usecase";

let userRepository: InMemoryUserRepository;
let createUserUseCase: CreateUserUseCase;

describe("Create user use case", () => {
  beforeEach(() => {
    userRepository = new InMemoryUserRepository();
    createUserUseCase = new CreateUserUseCase(userRepository);
  });

  it("should to create", async () => {
    const role = await createUserUseCase.create({
      id: randomUUID(),
      name: "Jhon",
      email: "jhon@mail.com",
      password: "123456",
      status: true,
      role_id: randomUUID(),
    });

    expect(role.id).toEqual(expect.any(String));
  });
});
