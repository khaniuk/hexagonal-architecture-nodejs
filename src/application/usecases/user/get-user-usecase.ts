import { User } from "@/domain/models/user";
import { GetUser } from "@/domain/ports/primary/user/get-user";
import { UserRepository } from "@/domain/ports/secondary/user-repository";

export class GetUserUseCase implements GetUser {
  constructor(private userRepository: UserRepository) {}

  async getAll(): Promise<Array<User>> {
    let users: Array<User> = [];

    const usersResponse = await this.userRepository.findAll();

    usersResponse.forEach((user) => {
      users.push({
        id: user.id,
        name: user.name,
        email: user.email,
        role_id: user.role_id,
        status: user.status,
        password: "",
      });
    });

    return users;
  }

  async getById(id: string): Promise<User> {
    const user = await this.userRepository.findById(id);

    if (!user) {
      throw Error("User not found");
    }

    return {
      id: user.id,
      name: user.name,
      email: user.email,
      role_id: user.role_id,
      status: user.status,
      password: "",
    };
  }
}
