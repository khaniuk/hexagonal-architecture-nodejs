import { EmailAlreadyExistsError } from "@/application/errors/email-already-exists-error";
import { User } from "@/domain/models/user";
import { CreateUser } from "@/domain/ports/primary/user/create-user";
import { UserRepository } from "@/domain/ports/secondary/user-repository";
import { hash } from "bcryptjs";

export class CreateUserUseCase implements CreateUser {
  constructor(private userRepository: UserRepository) {}

  async create({ name, email, password, role_id }: User): Promise<User> {
    if (!password) {
      throw new Error("Password es required");
    }

    const password_hash = await hash(password, 6);

    const userWithSameEmail = await this.userRepository.findByEmail(email);

    if (userWithSameEmail) {
      throw new EmailAlreadyExistsError();
    }

    const user = await this.userRepository.create({
      name,
      email,
      password: password_hash,
      role: {
        connect: {
          id: role_id,
        },
      },
    });

    return {
      id: user.id,
      name: user.name,
      email: user.email,
      role_id: user.role_id,
      status: user.status,
      password: "",
    };
  }
}
