import { InMemoryUserRepository } from "@/infrastructure/repositories/in-memory/in-memory-user-repository";
import { randomUUID } from "crypto";
import { beforeEach, describe, expect, it } from "vitest";
import { CreateUserUseCase } from "./create-user-usecase";
import { GetUserUseCase } from "./get-user-usecase";

let userRepository: InMemoryUserRepository;
let createUserUseCase: CreateUserUseCase;
let getUserUseCase: GetUserUseCase;

describe("Get role use case", () => {
  beforeEach(() => {
    userRepository = new InMemoryUserRepository();
    createUserUseCase = new CreateUserUseCase(userRepository);
    getUserUseCase = new GetUserUseCase(userRepository);
  });

  it("should to get users", async () => {
    await createUserUseCase.create({
      id: randomUUID(),
      name: "Jhon",
      email: "jhon@mail.com",
      password: "123456",
      status: true,
      role_id: randomUUID(),
    });

    await createUserUseCase.create({
      id: randomUUID(),
      name: "Jenn",
      email: "jenn@mail.com",
      password: "123456",
      status: true,
      role_id: randomUUID(),
    });

    const users = await getUserUseCase.getAll();

    expect(users).toHaveLength(2);
  });

  it("should to get user", async () => {
    const newRole = await createUserUseCase.create({
      id: randomUUID(),
      name: "Jhon",
      email: "jhon@mail.com",
      password: "123456",
      status: true,
      role_id: randomUUID(),
    });

    const id: string = newRole.id + "";
    const role = await getUserUseCase.getById(id);

    expect(role.email).toEqual("jhon@mail.com");
  });
});
