import { InMemoryUserRepository } from "@/infrastructure/repositories/in-memory/in-memory-user-repository";
import { randomUUID } from "crypto";
import { beforeEach, describe, expect, it } from "vitest";
import { CreateUserUseCase } from "./create-user-usecase";
import { DeleteUserUseCase } from "./delete-user-usecase";
import { GetUserUseCase } from "./get-user-usecase";

let userRepository: InMemoryUserRepository;
let createUserUseCase: CreateUserUseCase;
let getUserUseCase: GetUserUseCase;
let deleteUserUseCase: DeleteUserUseCase;

describe("Delete user use case", () => {
  beforeEach(() => {
    userRepository = new InMemoryUserRepository();
    createUserUseCase = new CreateUserUseCase(userRepository);
    getUserUseCase = new GetUserUseCase(userRepository);
    deleteUserUseCase = new DeleteUserUseCase(userRepository);
  });

  it("should to delete user", async () => {
    let user = await createUserUseCase.create({
      id: randomUUID(),
      name: "Jhon",
      email: "jhon@mail.com",
      password: "123456",
      status: true,
      role_id: randomUUID(),
    });

    const id: string = user.id + "";

    await deleteUserUseCase.delete(id);

    try {
      user = await getUserUseCase.getById(id);

      expect(user.id).toEqual(id);
    } catch (error) {
      if (error instanceof Error) {
        console.error(error.message);
        expect(error.message).toEqual("User not found");
      }
    }
  });
});
