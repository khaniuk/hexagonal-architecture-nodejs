import { EmailAlreadyExistsError } from "@/application/errors/email-already-exists-error";
import { User } from "@/domain/models/user";
import { UpdateUser } from "@/domain/ports/primary/user/update-user";
import { UserRepository } from "@/domain/ports/secondary/user-repository";
import { hash } from "bcryptjs";

export class UpdateUserUseCase implements UpdateUser {
  constructor(private userRepository: UserRepository) {}

  async update({
    id,
    name,
    email,
    password,
    role_id,
    status,
  }: User): Promise<User> {
    if (!id) {
      throw Error("User identify not valid");
    }

    const userResult = await this.userRepository.findById(id);
    if (userResult?.email != email) {
      const userWithSameEmail = await this.userRepository.findByEmail(email);

      if (userWithSameEmail) {
        throw new EmailAlreadyExistsError();
      }
    }

    let passwordHash = "";
    if (password) {
      passwordHash = await hash(password, 6);
    }

    const user = await this.userRepository.update(id, {
      name,
      email,
      password: passwordHash,
      role: {
        connect: {
          id: role_id,
        },
      },
      status,
    });

    return {
      id: user.id,
      name: user.name,
      email: user.email,
      password: "",
      role_id: user.role_id,
      status: user.status,
    };
  }
}
