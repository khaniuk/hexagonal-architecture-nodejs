import { InMemoryRoleRepository } from "@/infrastructure/repositories/in-memory/in-memory-role-repository";
import { randomUUID } from "crypto";
import { beforeEach, describe, expect, it } from "vitest";
import { CreateRoleUseCase } from "./create-role-usecase";

let roleRepository: InMemoryRoleRepository;
let createRoleUseCase: CreateRoleUseCase;

describe("Create role use case", () => {
  beforeEach(() => {
    roleRepository = new InMemoryRoleRepository();
    createRoleUseCase = new CreateRoleUseCase(roleRepository);
  });

  it("should to create", async () => {
    const role = await createRoleUseCase.create({
      id: randomUUID(),
      description: "Manager",
      status: true,
    });

    expect(role.id).toEqual(expect.any(String));
  });
});
