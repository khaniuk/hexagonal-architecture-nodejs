import { Role } from "@/domain/models/role";
import { UpdateRole } from "@/domain/ports/primary/role/update-role";
import { RoleRepository } from "@/domain/ports/secondary/role-repository";

export class UpdateRoleUseCase implements UpdateRole {
  constructor(private roleRepository: RoleRepository) {}

  async update({ id, description, status }: Role): Promise<Role> {
    if (!id) {
      throw Error("Role id not valid");
    }

    const user = await this.roleRepository.update({
      id,
      description,
      status,
    });

    return {
      id: user.id,
      description: user.description,
      status: user.status,
    };
  }
}
