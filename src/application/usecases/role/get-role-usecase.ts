import { Role } from "@/domain/models/role";
import { GetRole } from "@/domain/ports/primary/role/get-role";
import { RoleRepository } from "@/domain/ports/secondary/role-repository";

export class GetRoleUseCase implements GetRole {
  constructor(private roleRepository: RoleRepository) {}

  async getAll(): Promise<Array<Role>> {
    let roles: Array<Role> = [];

    const rolesResponse = await this.roleRepository.findAll();

    rolesResponse.forEach((role) => {
      roles.push({
        id: role.id,
        description: role.description,
        status: role.status,
      });
    });

    return roles;
  }

  async getById(id: string): Promise<Role> {
    const role = await this.roleRepository.findById(id);

    if (!role) {
      throw Error("Role not found");
    }

    return {
      id: role.id,
      description: role.description,
      status: role.status,
    };
  }
}
