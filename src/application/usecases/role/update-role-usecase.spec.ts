import { InMemoryRoleRepository } from "@/infrastructure/repositories/in-memory/in-memory-role-repository";
import { randomUUID } from "crypto";
import { beforeEach, describe, expect, it } from "vitest";
import { CreateRoleUseCase } from "./create-role-usecase";
import { UpdateRoleUseCase } from "./update-role-usecase";

let roleRepository: InMemoryRoleRepository;
let createRoleUseCase: CreateRoleUseCase;
let updateRoleUseCase: UpdateRoleUseCase;

describe("Update role use case", () => {
  beforeEach(() => {
    roleRepository = new InMemoryRoleRepository();
    createRoleUseCase = new CreateRoleUseCase(roleRepository);
    updateRoleUseCase = new UpdateRoleUseCase(roleRepository);
  });

  it("should to update", async () => {
    const newRole = await createRoleUseCase.create({
      id: randomUUID(),
      description: "Manager",
      status: true,
    });

    const role = await updateRoleUseCase.update({
      id: newRole.id,
      description: "Operator",
      status: true,
    });

    expect(role.description).toEqual("Operator");
  });
});
