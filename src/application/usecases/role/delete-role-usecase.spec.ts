import { InMemoryRoleRepository } from "@/infrastructure/repositories/in-memory/in-memory-role-repository";
import { randomUUID } from "crypto";
import { beforeEach, describe, expect, it } from "vitest";
import { CreateRoleUseCase } from "./create-role-usecase";
import { DeleteRoleUseCase } from "./delete-role-usecase";
import { GetRoleUseCase } from "./get-role-usecase";

let roleRepository: InMemoryRoleRepository;
let createRoleUseCase: CreateRoleUseCase;
let getRoleUseCase: GetRoleUseCase;
let deleteRoleUseCase: DeleteRoleUseCase;

describe("Delete role use case", () => {
  beforeEach(() => {
    roleRepository = new InMemoryRoleRepository();
    createRoleUseCase = new CreateRoleUseCase(roleRepository);
    getRoleUseCase = new GetRoleUseCase(roleRepository);
    deleteRoleUseCase = new DeleteRoleUseCase(roleRepository);
  });

  it("should to delete role", async () => {
    let role = await createRoleUseCase.create({
      id: randomUUID(),
      description: "Manager",
      status: true,
    });

    const id: string = role.id + "";

    await deleteRoleUseCase.delete(id);

    try {
      role = await getRoleUseCase.getById(id);

      expect(role.id).toEqual(id);
    } catch (error) {
      if (error instanceof Error) {
        console.error(error.message);
        expect(error.message).toEqual("Role not found");
      }
    }
  });
});
