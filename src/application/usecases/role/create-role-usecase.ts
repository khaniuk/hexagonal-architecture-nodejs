import { Role } from "@/domain/models/role";
import { CreateRole } from "@/domain/ports/primary/role/create-role";
import { RoleRepository } from "@/domain/ports/secondary/role-repository";

export class CreateRoleUseCase implements CreateRole {
  constructor(private roleRepository: RoleRepository) {}

  async create({ description }: Role): Promise<Role> {
    if (!description) {
      throw new Error("Description not valid");
    }

    const role = await this.roleRepository.create({
      description,
    });

    return {
      id: role.id,
      description: role.description,
      status: role.status,
    };
  }
}
