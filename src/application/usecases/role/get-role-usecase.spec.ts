import { InMemoryRoleRepository } from "@/infrastructure/repositories/in-memory/in-memory-role-repository";
import { randomUUID } from "crypto";
import { beforeEach, describe, expect, it } from "vitest";
import { CreateRoleUseCase } from "./create-role-usecase";
import { GetRoleUseCase } from "./get-role-usecase";

let roleRepository: InMemoryRoleRepository;
let createRoleUseCase: CreateRoleUseCase;
let getRoleUseCase: GetRoleUseCase;

describe("Get role use case", () => {
  beforeEach(() => {
    roleRepository = new InMemoryRoleRepository();
    createRoleUseCase = new CreateRoleUseCase(roleRepository);
    getRoleUseCase = new GetRoleUseCase(roleRepository);
  });

  it("should to get roles", async () => {
    await createRoleUseCase.create({
      id: randomUUID(),
      description: "Manager",
      status: true,
    });

    await createRoleUseCase.create({
      id: randomUUID(),
      description: "Operator",
      status: true,
    });

    const roles = await getRoleUseCase.getAll();

    expect(roles).toHaveLength(2);
  });

  it("should to get role", async () => {
    const newRole = await createRoleUseCase.create({
      id: randomUUID(),
      description: "Manager",
      status: true,
    });

    const id: string = newRole.id + "";
    const role = await getRoleUseCase.getById(id);

    expect(role.description).toEqual("Manager");
  });
});
