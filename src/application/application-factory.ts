import { RepositoryFactory } from "@/infrastructure/repositories/repository-factory";

import { CreateRoleUseCase } from "./usecases/role/create-role-usecase";
import { DeleteRoleUseCase } from "./usecases/role/delete-role-usecase";
import { GetRoleUseCase } from "./usecases/role/get-role-usecase";
import { UpdateRoleUseCase } from "./usecases/role/update-role-usecase";

import { CreateUserUseCase } from "./usecases/user/create-user-usecase";
import { DeleteUserUseCase } from "./usecases/user/delete-user-usecase";
import { GetUserUseCase } from "./usecases/user/get-user-usecase";
import { UpdateUserUseCase } from "./usecases/user/update-user-usecase";

export class ApplicationFactory {
  private static roleRepository = RepositoryFactory.roleRepository();
  private static userRepository = RepositoryFactory.userRepository();

  public static getRoleUseCase() {
    const getRole = new GetRoleUseCase(this.roleRepository);

    return getRole;
  }

  public static createRoleUseCase() {
    const createRole = new CreateRoleUseCase(this.roleRepository);

    return createRole;
  }

  public static updateRoleUseCase() {
    const updateRole = new UpdateRoleUseCase(this.roleRepository);

    return updateRole;
  }

  public static deleteRoleUseCase() {
    const deleteRole = new DeleteRoleUseCase(this.roleRepository);

    return deleteRole;
  }

  public static getUserUseCase() {
    const getUser = new GetUserUseCase(this.userRepository);

    return getUser;
  }

  public static createUserUseCase() {
    const createUser = new CreateUserUseCase(this.userRepository);

    return createUser;
  }

  public static updateUserUseCase() {
    const updateUser = new UpdateUserUseCase(this.userRepository);

    return updateUser;
  }

  public static deleteUserUseCase() {
    const deleteUser = new DeleteUserUseCase(this.userRepository);

    return deleteUser;
  }
}
