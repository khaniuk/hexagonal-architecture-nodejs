import { User } from "@/domain/models/user";

export interface CreateUser {
  create(data: User): Promise<User>;
}
