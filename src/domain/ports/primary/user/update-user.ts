import { User } from "@/domain/models/user";

export interface UpdateUser {
  update(data: User): Promise<User>;
}
