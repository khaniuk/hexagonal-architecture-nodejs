import { Role } from "@/domain/models/role";

export interface GetRole {
  getAll(): Promise<Array<Role>>;

  getById(id: string): Promise<Role>;
}
