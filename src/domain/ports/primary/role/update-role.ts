import { Role } from "@/domain/models/role";

export interface UpdateRole {
  update(data: Role): Promise<Role>;
}
