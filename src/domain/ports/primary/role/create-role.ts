import { Role } from "@/domain/models/role";

export interface CreateRole {
  create(data: Role): Promise<Role>;
}
