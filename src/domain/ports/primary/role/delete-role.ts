export interface DeleteRole {
  delete(id: string): Promise<void>;
}
